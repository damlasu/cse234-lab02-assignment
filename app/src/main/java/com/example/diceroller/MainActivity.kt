package com.example.diceroller

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import java.time.temporal.ValueRange

/**
 * This activity allows a user to roll a pair of dice and view the result on the screen
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Button component assigned to the rollButton
        val rollButton: Button = findViewById(R.id.button)

        //when you clicked to roll button setOnClickListener will be activated
        rollButton.setOnClickListener {
            rollDice()
            val toast = Toast.makeText(this, "Dice Rolled!", Toast.LENGTH_SHORT)
            toast.show()
        }
        rollDice() //to show initial image at the start

    }

    /**
     * Roll the dice and update the screen with the result
     */
    private fun rollDice() {
        val dice = Dice(6)
        val diceRoll = dice.roll()
        val diceRoll2 = dice.roll()

        val diceImage: ImageView = findViewById(R.id.roll_image1)
        val drawableResource = dice.rollImageCont(diceRoll)
        diceImage.setImageResource(drawableResource)

        val diceImage2: ImageView = findViewById(R.id.roll_image2)
        val drawableResource2 = dice.rollImageCont(diceRoll2)
        diceImage2.setImageResource(drawableResource2)
    }

    /**
     * Creates dice class
     * roll() function to roll a number randomly
     * rollImageCont(diceRoll:Int) function to assign the image for a rolled number
     */
    class Dice(private val numSides: Int) {
        fun roll(): Int {
            return (1..numSides).random()
        }

        fun rollImageCont(diceRoll: Int): Int {
            //switch case can be used as when in kotlin
            val drawableResource = when (diceRoll) {
                1 -> R.drawable.dice_1
                2 -> R.drawable.dice_2
                3 -> R.drawable.dice_3
                4 -> R.drawable.dice_4
                5 -> R.drawable.dice_5
                else -> R.drawable.dice_6  //to avoid from an error
            }
            return drawableResource
        }
    }
}